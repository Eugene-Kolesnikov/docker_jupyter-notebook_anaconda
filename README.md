# Docker_Jupyter notebook_anaconda

The repository provides a setup for a Dockerized Jupyter notebook with anaconda.

## Build

In order to build the docker container, run the command:

```
docker-compose build
```

## Start the notebook container

In order to start the notebook container, run the command:

```
docker-compose up
```

The terminal will display the link which must be use to access the notebook. The final url has the form:

```
http://localhost:8888/?token=bfdd029b3ece7094fb...
```

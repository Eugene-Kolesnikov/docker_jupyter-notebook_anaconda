FROM continuumio/miniconda3

ENV PYTHONUNBUFFERED 1
RUN mkdir /home/notebooks
COPY ./notebooks /home/notebooks
WORKDIR /home/notebooks

RUN apt-get update && apt-get install -y && apt-get install build-essential libssl-dev -y
RUN conda create --name CompStat pip python=3.6 notebook tini numpy matplotlib scipy
RUN [ "/bin/bash", "-c", ". /opt/conda/etc/profile.d/conda.sh && conda activate CompStat && conda install -c conda-forge tensorflow=1.4 corner && pip install edward"]
